package br.com.desafio.javaspringboot.model.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import br.com.desafio.javaspringboot.domain.model.Product;
import br.com.desafio.javaspringboot.domain.repository.ProductRepository;


@SpringBootTest
public class ProdutoRepositoryTest {

    @Autowired
    private ProductRepository produtoRepository;

    @Test
    public void deve_salvar_um_produto(){
    	
        Product p1 = new Product();
        p1.setName("Escova de dente");
        p1.setDescription("Usada para higiene bucal");
        p1.setPrice(10.5);
        
        produtoRepository.save(p1);
        
        assertNotNull(p1.getId());
        
    }

    @Test
    public void deve_atualizar_o_preco_do_produto(){
    	
    	double oldPrice = 2.50;
    	double newPrice = 3.50;
    	
    	Product p1 = new Product();
        p1.setName("Leite");
        p1.setDescription("Leite integral");
        p1.setPrice(oldPrice);
        
        produtoRepository.save(p1);
        p1.setPrice(newPrice);
        produtoRepository.save(p1);
        
        Product p2 = produtoRepository.findById(p1.getId()).get();
        assertEquals(p2.getPrice(), newPrice);
    }

    @Test
    public void deve_remover_um_produto(){
    	
    	Product p1 = new Product();
        p1.setName("Canela");
        p1.setDescription("Canela em pó");
        p1.setPrice(0.50);
        
        produtoRepository.save(p1);
        
        assertNotNull(p1.getId());
        
        produtoRepository.delete(p1);

        Product p2 = produtoRepository.findById(p1.getId()).orElse(null);
        
        assertNull(p2);
    }

    @Test
    public void deve_listar_tres_ou_mais_produtos(){
    	
    	produtoRepository.deleteAll(produtoRepository.findAll());
    	
    	Product p1 = new Product();
    	p1.setName("Desodorante");
        p1.setDescription("Desodorante refrescante");
        p1.setPrice(11.18);
        
        Product p2 = new Product();
        p2.setName("Feijão");
        p2.setDescription("Feijão carioca");
        p2.setPrice(7.05);
        
        Product p3 = new Product();
        p3.setName("Arroz");
        p3.setDescription("Arroz Branco tipo 2");
        p3.setPrice(12.50);
         
        produtoRepository.saveAll(List.of(p1, p2, p3));
        
        List<Product> produtos = produtoRepository.findAll();
        
        assertTrue(produtos.size() >= 3);
        

    }
    
    @DirtiesContext
    @Test
    public void deve_listar_produtos_que_o_preco_seja_maior_ou_igual_a_dez(){
    	
    	produtoRepository.deleteAll(produtoRepository.findAll());
    	
    	Product p1 = new Product();
        p1.setName("Desodorante");
        p1.setDescription("Desodorante refrescante");
        p1.setPrice(11.18);
        
        Product p2 = new Product();
        p2.setName("Feijão");
        p2.setDescription("Feijão carioca");
        p2.setPrice(7.05);
        
        Product p3 = new Product();
        p3.setName("Arroz");
        p3.setDescription("Arroz Branco tipo 2");
        p3.setPrice(12.50);
        
        Product p4 = new Product();
        p4.setName("Maça");
        p4.setDescription("Maça tipo Gala");
        p4.setPrice(5.60);
        
        Product p5 = new Product();
        p5.setName("Banana");
        p5.setDescription("Banana nanica");
        p5.setPrice(8.50);
        
        Product p6 = new Product();
        p6.setName("Torrone");
        p6.setDescription("Torrone de amendoim");
        p6.setPrice(2.20);

        produtoRepository.saveAll(List.of(p1, p2, p3, p4, p5, p6));
        
        double minValue = 10.0;
        List<Product> produtos = produtoRepository.searchProdutos(null, minValue, null);
        assertTrue(produtos.size() == 2);
        
        List<Product> produtosLessthanTen = produtos.stream().filter( p -> p.getPrice() < 10 ).collect(Collectors.toList());
        assertTrue(produtosLessthanTen.size() == 0);
    }

    @DirtiesContext
    @Test
    public void deve_listar_produtos_que_o_preco_seja_menor_ou_igual_a_dez(){
    	
    	produtoRepository.deleteAll(produtoRepository.findAll());
    	
    	Product p1 = new Product();
        p1.setName("Desodorante");
        p1.setDescription("Desodorante refrescante");
        p1.setPrice(11.18);
        
        Product p2 = new Product();
        p2.setName("Feijão");
        p2.setDescription("Feijão carioca");
        p2.setPrice(7.05);
        
        Product p3 = new Product();
        p3.setName("Arroz");
        p3.setDescription("Arroz Branco tipo 2");
        p3.setPrice(12.50);
        
        Product p4 = new Product();
        p4.setName("Maça");
        p4.setDescription("Maça tipo Gala");
        p4.setPrice(5.60);
        
        Product p5 = new Product();
        p5.setName("Banana");
        p5.setDescription("Banana nanica");
        p5.setPrice(8.50);
        
        Product p6 = new Product();
        p6.setName("Torrone");
        p6.setDescription("Torrone de amendoim");
        p6.setPrice(2.20);

        produtoRepository.saveAll(List.of(p1, p2, p3, p4, p5, p6));
        
        double maxValue = 10.0;
        List<Product> produtos = produtoRepository.searchProdutos(null, null, maxValue);
        assertTrue(produtos.size() == 4);
        
        List<Product> produtosGreaterthanTen = produtos.stream().filter( p -> p.getPrice() > 10 ).collect(Collectors.toList());
        assertTrue(produtosGreaterthanTen.size() == 0);
    }

    @DirtiesContext
    @Test
    public void deve_listar_produtos_que_os_campos_name_e_description_contenham_o_texto_brinq(){
    
    	produtoRepository.deleteAll(produtoRepository.findAll());

    	Product p1 = new Product();
        p1.setName("Desodorante");
        p1.setDescription("Desodorante refrescante");
        p1.setPrice(11.18);
        
        Product p2 = new Product();
        p2.setName("Feijão");
        p2.setDescription("Feijão carioca");
        p2.setPrice(7.05);
        
        Product p3 = new Product();
        p3.setName("Arroz");
        p3.setDescription("Arroz Branco tipo 2");
        p3.setPrice(12.50);
        
        Product p4 = new Product();
        p4.setName("Maça");
        p4.setDescription("Maça tipo Gala");
        p4.setPrice(5.60);
        
        Product p5 = new Product();
        p5.setName("Banana");
        p5.setDescription("Banana nanica");
        p5.setPrice(8.50);
        
        Product p6 = new Product();
        p6.setName("Torrone");
        p6.setDescription("Torrone de amendoim e banana");
        p6.setPrice(2.20);

        produtoRepository.saveAll(List.of(p1, p2, p3, p4, p5, p6));
        
        String q = "bana";
    
        List<Product> produtos = produtoRepository.searchProdutos(q, null, null);
        produtos.forEach(System.out::println);
        assertEquals(2, produtos.size());      
        
    }


}
