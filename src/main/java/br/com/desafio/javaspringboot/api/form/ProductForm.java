package br.com.desafio.javaspringboot.api.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

import br.com.desafio.javaspringboot.domain.model.Product;
import br.com.desafio.javaspringboot.domain.service.ProductService;
import io.swagger.annotations.ApiModel;

@ApiModel("ProductForm")
public class ProductForm {
	
	@NotEmpty
	private String name;
	@NotEmpty
	private String description;
	@Positive
	private double price;
	
	public Product toProduto() {
		Product produto = new Product();
		produto.setName(this.name);
		produto.setDescription(this.description);
		produto.setPrice(this.price);
		return produto;
	}
	
	public Product toProduto(Long id, ProductService produtoService) {
		Product produto = produtoService.findById(id);
		produto.setName(this.name);
		produto.setDescription(this.description);
		produto.setPrice(this.price);
		
		return produto;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(name != null) {
			name = name.trim();
		}
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if(description != null) {
			description = description.trim();
		}
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	
	
}
