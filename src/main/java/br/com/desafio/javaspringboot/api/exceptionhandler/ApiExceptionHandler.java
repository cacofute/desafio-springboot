package br.com.desafio.javaspringboot.api.exceptionhandler;

import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import br.com.desafio.javaspringboot.domain.exception.EntityNotFoundException;
import br.com.desafio.javaspringboot.domain.exception.ProductNotFoundException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler	{
	
	private static final String MSG_REQUEST_FORMAT_INVALID = "A propriedade '%s' recebeu o valor '%s', "
			+ "que é de um tipo inválido. "
			+ "Corrija e informe um valor compatível com o tipo %s";
	
	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<?> handleEntidadeNaoEncontradaException(EntityNotFoundException ex, 
			WebRequest request){
		
		HttpStatus status = HttpStatus.NOT_FOUND;
		Problem problema = Problem
				.builder()
				.statusCode(status.value())
				.message(ex.getMessage()).build();
		
		return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
	}
		
	
	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<?> handleProdutoNaoEncontradoException(ProductNotFoundException ex,
			WebRequest request){
		
		HttpStatus status = HttpStatus.NOT_FOUND;
		Problem problema = Problem
				.builder()
				.statusCode(status.value())
				.message(ex.getMessage()).build();
		
		return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Throwable rootCause = ExceptionUtils.getRootCause(ex);
		if(rootCause instanceof InvalidFormatException) {
			return handleInvalidFormatException((InvalidFormatException) rootCause, headers, status, request);
		}
		
		Problem problema = Problem
				  .builder()
				  .statusCode(status.value())
				  .message(status.getReasonPhrase()).build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleUnknowException(Exception ex, WebRequest request){
		
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		Problem problema = Problem
				.builder()
				.statusCode(status.value())
				.message("Server internal error").build();
		
		return handleExceptionInternal(ex, problema, new HttpHeaders(), status, request);
	}
	
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Optional<FieldError> optionalFieldError =  ex.getBindingResult()
			.getFieldErrors()
		    .stream()
			.findFirst();
		
		String message = "Dados inválidos";
		if(optionalFieldError.isPresent()) {
			FieldError fieldError = optionalFieldError.get();
			message = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
		}
		
		Problem problema = Problem
				.builder()
				.statusCode(status.value())
				.message(message).build();
		
		return handleExceptionInternal(ex, problema, headers, status, request);
	}
	
	
	/**
	 * 
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		String path = ex.getPath().stream()
				.map( ref -> ref.getFieldName() )
				.collect(Collectors.joining("."));
		
		String detail = String.format(MSG_REQUEST_FORMAT_INVALID, 
				path, 
				ex.getValue(), 
				ex.getTargetType().getSimpleName());
		
		Problem problema = Problem
				  .builder()
				  .statusCode(status.value())
				  .message(detail).build();
		return handleExceptionInternal(ex, problema, headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		if(body == null) {
			body = Problem
			  .builder()
			  .statusCode(status.value())
			  .message(status.getReasonPhrase()).build();
		}
		else if(body instanceof String) {
			body = Problem
			   .builder()
			   .statusCode(status.value())
			   .message((String) body).build();
		}
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	

}
