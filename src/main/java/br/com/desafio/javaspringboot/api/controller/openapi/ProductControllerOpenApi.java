package br.com.desafio.javaspringboot.api.controller.openapi;


import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.desafio.javaspringboot.api.dto.ProductDTO;
import br.com.desafio.javaspringboot.api.exceptionhandler.Problem;
import br.com.desafio.javaspringboot.api.form.ProductForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Produtos")
public interface ProductControllerOpenApi {
		
	@ApiOperation(value = "Lista de produtos")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Lista de produtos"),
	})
	ResponseEntity<List<ProductDTO>> findAll();

	@ApiOperation(value = "Criação de um produto")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Produto criado")
	})
	ResponseEntity<ProductDTO> save(@RequestBody @Valid ProductForm productForm);
	
	@ApiOperation(value = "Atualização de um produto")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produto atualizado"),
			@ApiResponse(code = 404, message = "Produto não localizado", response = Problem.class)
	})
	ResponseEntity<ProductDTO> update(@PathVariable Long id, @RequestBody @Valid ProductForm productForm);
	
	@ApiOperation(value = "Deleção de um produto")
	@ApiResponses(value = {
			@ApiResponse(code = 404, message = "Produto não localizado", response = Problem.class)
	})
	@DeleteMapping(value = "/{id}")
	void delete(Long id);

	@ApiOperation(value = "Busca de um produto por ID")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produto localizado"),
			@ApiResponse(code = 404, message = "Produto não localizado", response = Problem.class)
	})
	ResponseEntity<ProductDTO> findById(Long id);
	
	@ApiOperation(value = "Lista de produtos filtrados")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Produtos encontrados"),
	})
	ResponseEntity<List<ProductDTO>> search( 
		    String valueSearched, Double minPrice, Double maxPrice);


}
