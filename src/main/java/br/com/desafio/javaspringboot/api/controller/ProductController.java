package br.com.desafio.javaspringboot.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.javaspringboot.api.controller.openapi.ProductControllerOpenApi;
import br.com.desafio.javaspringboot.api.dto.ProductDTO;
import br.com.desafio.javaspringboot.api.form.ProductForm;
import br.com.desafio.javaspringboot.domain.model.Product;
import br.com.desafio.javaspringboot.domain.service.ProductService;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "*")
public class ProductController implements ProductControllerOpenApi{
	
	private ProductService productService;
	
	public ProductController(ProductService produtoService) {
		this.productService = produtoService;
	}
	
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<ProductDTO>> findAll(){
		List<ProductDTO> productsDTO = productService
				.findAll()
				.stream()
				.map( ProductDTO::new )
				.collect(Collectors.toList());
		return ResponseEntity.ok(productsDTO);
	}

	
	@PostMapping(produces = "application/json")
	public ResponseEntity<ProductDTO> save(@RequestBody @Valid ProductForm productForm){
		Product product    = productForm.toProduto();
		Product newProduct = productService.save(product);
		return ResponseEntity.status(HttpStatus.CREATED).body(new ProductDTO(newProduct));	
	}
	
	
	@PutMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<ProductDTO> update(@PathVariable Long id, @RequestBody @Valid ProductForm productForm){
		Product produto        = productForm.toProduto(id, productService);
		Product productUpdated = productService.update(produto);
		return ResponseEntity.ok(new ProductDTO(productUpdated));
	}
	
	
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK) 
	public void delete(@PathVariable Long id){
		productService.deleteById(id);
	}

	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<ProductDTO> findById(@PathVariable Long id){
		Product product = productService.findById(id);
		return ResponseEntity.ok(new ProductDTO(product));
	
	}
	
	
	@GetMapping(value = "/search", produces = "application/json")
	public ResponseEntity<List<ProductDTO>> search( 
			@RequestParam(name = "q", required = false) String valueSearched,
			@RequestParam(name = "min_price", required = false) Double minPrice, 
			@RequestParam(name = "max_price", required = false) Double maxPrice){
		
		 List<ProductDTO> productsDTO = productService.searchProdutos(valueSearched, minPrice, maxPrice)
			.stream()
		 	.map(ProductDTO::new)
		 	.collect(Collectors.toList()); 
		 
		 return ResponseEntity.ok(productsDTO);
	}
	
}
