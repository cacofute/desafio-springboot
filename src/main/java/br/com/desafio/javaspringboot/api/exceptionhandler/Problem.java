package br.com.desafio.javaspringboot.api.exceptionhandler;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

@ApiModel("Problem")
@Getter
@Builder
public class Problem {

	@JsonProperty("status_code")
	private Integer statusCode;
	private String message;
}
