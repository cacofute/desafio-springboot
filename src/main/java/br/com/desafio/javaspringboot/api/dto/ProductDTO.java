package br.com.desafio.javaspringboot.api.dto;

import br.com.desafio.javaspringboot.domain.model.Product;
import io.swagger.annotations.ApiModel;


@ApiModel("ProductDTO")
public class ProductDTO {
	
	private Long id;
	private String name;
	private String description;
	private double price;
	
	public ProductDTO(Product produto) {
		this.id          = produto.getId();
		this.name        = produto.getName();
		this.description = produto.getDescription();
		this.price       = produto.getPrice();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	
}
