package br.com.desafio.javaspringboot.infrastructure.repository.spec;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import br.com.desafio.javaspringboot.domain.model.Product;

public class ProductSpec {
	
	/**
	 * 
	 * @param valor
	 * @return
	 */
	public static Specification<Product> comPrecoDoProdutoMaiorOuIgual(Double valor){
		return (root, query, builder) -> 
			builder.greaterThanOrEqualTo(root.get("price"), valor);
	}
	
	/**
	 * 
	 * @param valor
	 * @return
	 */
	public static Specification<Product> comPrecoDoProdutoMenorOuIgual(Double valor){
		return (root, query, builder) -> 
			builder.lessThanOrEqualTo(root.get("price"), valor);
	}

	/**
	 * 
	 * @param valor
	 * @return
	 */
	public static Specification<Product> comCampoNameOuDescriptionLike(String valor){
		return (root, query, builder) -> {
			Predicate namePredicate              = builder.like(builder.lower(root.get("name")), "%" + valor + "%");
			Predicate descriptionPredicate       = builder.like(builder.lower(root.get("description")), "%" + valor + "%");
			Predicate nameOrDescriptionPredicate = builder.or(namePredicate, descriptionPredicate);
			return nameOrDescriptionPredicate;
		};		
	}
	
}
