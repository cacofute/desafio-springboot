package br.com.desafio.javaspringboot.infrastructure.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import br.com.desafio.javaspringboot.domain.model.Product;
import br.com.desafio.javaspringboot.domain.repository.ProductRepository;
import br.com.desafio.javaspringboot.domain.repository.ProductRepositoryQueries;

@Repository
public class ProductRepositoryImpl implements ProductRepositoryQueries{
	
	@PersistenceContext
	private EntityManager manager;
	
	private ProductRepository produtoRepository;
	
	@Lazy
	public ProductRepositoryImpl(ProductRepository produtoRepository) {
		this.produtoRepository = produtoRepository;
	}

	@Override
	public List<Product> searchProdutos(String valueSearch, Double minPrice, Double maxPrice) {
		
		var criteriaBuilder = manager.getCriteriaBuilder();
		var criteria        = criteriaBuilder.createQuery(Product.class);
		var root            = criteria.from(Product.class);
		var predicates      = new ArrayList<>();
		
		if(StringUtils.hasText(valueSearch)) {
			valueSearch 						 = valueSearch.toLowerCase();
			Predicate namePredicate              = criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + valueSearch + "%");
			Predicate descriptionPredicate       = criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + valueSearch + "%");
			Predicate nameOrDescriptionPredicate = criteriaBuilder.or(namePredicate, descriptionPredicate);
			predicates.add(nameOrDescriptionPredicate);
		}
		
		if(minPrice != null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), minPrice));
		}
		
		if(maxPrice != null) {
			predicates.add( criteriaBuilder.lessThanOrEqualTo(root.get("price"), maxPrice));
		}
		
		criteria.where(predicates.toArray(new Predicate[0]));
		var typedQuery = manager.createQuery(criteria);
		
		return typedQuery.getResultList();
	}

}
