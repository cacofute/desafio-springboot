package br.com.desafio.javaspringboot.infrastructure.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.desafio.javaspringboot.domain.exception.ProductNotFoundException;
import br.com.desafio.javaspringboot.domain.model.Product;
import br.com.desafio.javaspringboot.domain.repository.ProductRepository;
import br.com.desafio.javaspringboot.domain.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	
	private ProductRepository produtoRepository;

	public ProductServiceImpl(ProductRepository produtoRepository) {
		this.produtoRepository = produtoRepository;
	}

	@Override
	public Product save(Product produto) {
		return produtoRepository.save(produto);
	}
	
	@Override
	public Product update(Product produto) {
		return save(produto);
	}

	@Override
	public void delete(Product produto) {
		produtoRepository.delete(produto);
	}
	
	@Override
	public void deleteById(Long id) {
		Product produto = findById(id);
		delete(produto);
	}

	@Override
	public Product findById(Long id) {
		return produtoRepository.findById(id)
			.orElseThrow(() -> new ProductNotFoundException(
					String.format("Não existe produto para o id %d", id)));
	}

	@Override
	public List<Product> findAll() {
		return produtoRepository.findAll();
	}

	@Override
	public List<Product> searchProdutos(String valueSearch, Double minPrice, Double maxPrice) {
		return produtoRepository.searchProdutos(valueSearch, minPrice, maxPrice);
	}

	


}
