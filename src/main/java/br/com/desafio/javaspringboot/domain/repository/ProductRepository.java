package br.com.desafio.javaspringboot.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.desafio.javaspringboot.domain.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, ProductRepositoryQueries, 
	JpaSpecificationExecutor<Product> {
	
	List<Product> findByPriceGreaterThanEqual(double minValue);
	
	List<Product> findByPriceLessThanEqual(double maxValue);
	
}
