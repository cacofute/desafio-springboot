package br.com.desafio.javaspringboot.domain.service;

import java.util.List;

import br.com.desafio.javaspringboot.domain.model.Product;

public interface ProductService extends ServiceDAO<Product, Long>{

	List<Product> searchProdutos(String valueSearch, Double minPrice, Double maxPrice);
}
