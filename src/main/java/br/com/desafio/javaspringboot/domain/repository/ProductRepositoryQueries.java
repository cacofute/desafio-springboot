package br.com.desafio.javaspringboot.domain.repository;

import java.util.List;

import br.com.desafio.javaspringboot.domain.model.Product;

public interface ProductRepositoryQueries {
	
	List<Product> searchProdutos(String valueSearch, Double minPrice, Double maxPrice);

}
