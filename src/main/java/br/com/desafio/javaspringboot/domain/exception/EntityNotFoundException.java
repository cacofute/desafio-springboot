package br.com.desafio.javaspringboot.domain.exception;

public abstract class EntityNotFoundException extends RuntimeException{

	private static final long serialVersionUID = -3843268230111540532L;

	public EntityNotFoundException(String message) {
		super(message);
		
	}
	
	

}
