package br.com.desafio.javaspringboot.domain.exception;

public class ProductNotFoundException extends EntityNotFoundException{

	private static final long serialVersionUID = 1400110243489210938L;
	
	public ProductNotFoundException(String message) {
		super(message);
	}


}
