# Desafio Java Spring Boot

## RestAPI de Catálogo de Produtos

## Como executar a aplicação.

Antes de começar, tenha certeza de configurar a variável de ambiente JAVA_HOME e o Maven no classpath.
Caso execute por algum IDE, verifique a instalação do projeto Lombok.

## Tecnologias utilizadas no projeto.

- Java 11 
- Apache Maven 3.6.3
- Spring Boot (version 1.5.6) 
- Lombok v1.18.20 
- Spring Tool Suite 4 Version: 4.11.0.RELEASE

Clone este repositório, depois entre na pasta raiz do projeto.

- execute: ./mvnw package

Depois rode o projeto com o comando abaixo

- java -jar target/java-springboot-0.0.1-SNAPSHOT.jar 

Caso não seja especificado em linha de comando, o projeto irá utilizar a porta: 9999

- exemplo: http://localhost:9999/products/


- Documentação da API: http://localhost:9999/swagger-ui.html

